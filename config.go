package main

import (
	_ "embed"
	"gopkg.in/yaml.v3"
	"time"
)

type (
	PathType string
)

var (
	DAY    PathType = "DAY"
	HOUR   PathType = "HOUR"
	SECOND PathType = "SECOND"
	NONE   PathType = "NONE"
)

type (
	Config struct {
		Type   BedType      `yaml:"type"`
		Github GithubConfig `yaml:"github"`
	}

	GithubConfig struct {
		Repo           string   `yaml:"repo"`
		Owner          string   `yaml:"owner"`
		Token          string   `yaml:"token"` // github write token
		Branch         string   `yaml:"branch"`
		PrefixPathType PathType `yaml:"prefix-path-type"`
		Commit         string   `yaml:"commit"`
		Author         Author   `yaml:"author"`
	}

	Author struct {
		Name string `yaml:"name"`
		Mail string `yaml:"mail"`
	}
)

//go:embed config.yaml
var configBlob []byte

var GlobalConfig Config

func InitConfig() {
	var config = Config{}
	err := yaml.Unmarshal(configBlob, &config)
	if err != nil {
		panic(err)
	}

	GlobalConfig = config
}

func (pt PathType) Path() string {
	switch pt {
	case DAY:
		return time.Now().Format("2006/01/02")
	case HOUR:
		return time.Now().Format("2006/01/02/15/04")
	case SECOND:
		return time.Now().Format("2006/01/02/15/04/05")
	case NONE:
		return ""
	default:
		return ""
	}
}
