package main

type (
	BedType string
)

type Bed interface {
	Type() BedType
	Init() error
	UploadByPath(filePath string) (string, error)
	UploadByBytes(bs []byte, fname string) (string, error)
}

var (
	strategyMap = make(map[BedType]Bed)
)

func Register(bedImpl Bed) {
	if strategyMap[bedImpl.Type()] != nil {
		return
	}

	strategyMap[bedImpl.Type()] = bedImpl
}

func FindStrategy(bedType BedType) Bed {
	return strategyMap[bedType]
}
