package main

import (
	"github.com/disintegration/imaging"
	"image/color"
	"testing"
)

func TestWriteWordMask(t *testing.T) {
	mask, err := PreWordMask(20, 100)
	if err != nil {
		panic(err)
	}

	bgImg, err := imaging.Open("../00038-275272746.png")
	if err != nil {
		panic(err)
	}
	wordMask := WriteWordMask("demo中文", mask, bgImg, 100, 100, color.White)

	err = imaging.Save(wordMask, "demo.png")
	if err != nil {
		panic(err)
	}
}
