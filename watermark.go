package main

import (
	_ "embed"
	"github.com/disintegration/imaging"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/font/sfnt"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
)

var (
	//go:embed SourceHanSansCN-Normal.otf
	SourceHanSansCNNormal []byte
)

func PreWordMask(size float64, dpi float64) (font.Face, error) {
	parse, err := sfnt.Parse(SourceHanSansCNNormal)
	if err != nil {
		return nil, err
	}
	return opentype.NewFace(parse, &opentype.FaceOptions{
		Size:    size,
		DPI:     dpi,
		Hinting: font.HintingNone,
	})
}

func WriteWordMask(word string, face font.Face, bgImg image.Image, x, y int, color color.Color) image.Image {
	drawer := font.Drawer{Face: face}
	drawer.Dot = fixed.P(x, y)
	drawer.Src = image.NewUniform(color)
	dstImg := image.NewRGBA(bgImg.Bounds())
	drawer.Dst = dstImg
	drawer.DrawString(word)

	return imaging.OverlayCenter(bgImg, dstImg, 1)
}

func parseHexColorFast(s string) color.RGBA {
	var c color.RGBA

	c.A = 0xff

	if s[0] != '#' {
		return c
	}

	switch len(s) {
	case 7:
		c.R = hexToByte(s[1])<<4 + hexToByte(s[2])
		c.G = hexToByte(s[3])<<4 + hexToByte(s[4])
		c.B = hexToByte(s[5])<<4 + hexToByte(s[6])
	case 4:
		c.R = hexToByte(s[1]) * 17
		c.G = hexToByte(s[2]) * 17
		c.B = hexToByte(s[3]) * 17
	}

	return c
}

func hexToByte(b byte) byte {
	switch {
	case b >= '0' && b <= '9':
		return b - '0'
	case b >= 'a' && b <= 'f':
		return b - 'a' + 10
	case b >= 'A' && b <= 'F':
		return b - 'A' + 10
	}

	return '0'
}
