package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"time"

	"github.com/google/go-github/v60/github"
	"golang.org/x/oauth2"
)

type (
	GithubBed struct {
		config GithubConfig
		client *github.Client
	}
)

func (g *GithubBed) Init() error {
	log.Printf(fmt.Sprintf("Init %s", g.Type()))

	ctx, df := context.WithTimeout(context.Background(), time.Second*30)
	defer df()

	githubConfig := GlobalConfig.Github
	err := g.Vailed(githubConfig)
	if err != nil {
		return err
	}

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: githubConfig.Token},
	)
	tc := oauth2.NewClient(ctx, ts)
	client := github.NewClient(tc)

	g.config = githubConfig
	g.client = client

	return nil
}

func (g *GithubBed) Type() BedType {
	return "github"
}

// UploadByPath impl bed.Bed
func (g *GithubBed) UploadByPath(filePath string) (string, error) {
	fi, err := os.Stat(filePath)
	if err != nil {
		return "", err
	}

	f, err := os.OpenFile(filePath, os.O_RDONLY, 0755)
	if err != nil {
		return "", err
	}

	fs, err := io.ReadAll(f)
	if err != nil {
		return "", err
	}

	return g.UploadByBytes(fs, fi.Name())
}

func (g *GithubBed) UploadByBytes(bs []byte, fileName string) (string, error) {
	log.Printf(fmt.Sprintf("Upload name %s", fileName))

	conf := g.config

	opts := &github.RepositoryContentFileOptions{
		Message:   github.String(fmt.Sprintf("%v(%v)", conf.Commit, time.Now().Format("2006-01-02 15:04:05"))),
		Content:   bs,
		Branch:    github.String(conf.Branch),
		Committer: &github.CommitAuthor{Name: github.String(conf.Author.Name), Email: github.String(conf.Author.Mail)},
	}

	resp, _, err := g.client.Repositories.CreateFile(context.Background(), conf.Owner, conf.Repo, path.Join(conf.PrefixPathType.Path(), fileName), opts)
	if err != nil {
		return "", err
	}

	return resp.Content.GetDownloadURL(), nil
}

func (g *GithubBed) Vailed(config GithubConfig) error {
	if config.Token == "" {
		return errors.New("token must not be empty")
	}

	return nil
}
