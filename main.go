package main

import (
	"errors"
	"flag"
	"io/fs"
	"log"
	"path/filepath"
	"strings"
)

func main() {
	InitConfig()

	Register(&GithubBed{})

	var (
		uploadDirPath  string
		uploadFilePath string
	)
	flag.StringVar(&uploadDirPath, "d", "", "upload dir files, example: d ./")
	flag.StringVar(&uploadFilePath, "p", "", "upload file path: example: p ./a.txt")
	flag.Parse()

	bedImpl := FindStrategy(GlobalConfig.Type)
	if bedImpl == nil {
		panic(errors.New("type not right"))
	}
	err := bedImpl.Init()
	if err != nil {
		panic(err)
	}

	// upload dir
	if uploadDirPath != "" {
		uploadDir(bedImpl, uploadDirPath)

		return
	}

	// upload sigle file
	if uploadFilePath != "" {
		upload(bedImpl, uploadFilePath)

		return
	}

	log.Println("use -h to get help")
}

// uploadDir upload dir file
func uploadDir(b Bed, baseDirPath string) (res []string) {
	filepath.Walk(baseDirPath, func(path string, info fs.FileInfo, err error) error {
		// ignore dir
		if info.IsDir() {
			return nil
		}

		// ignore hidden file
		if strings.HasPrefix(info.Name(), ".") || strings.HasPrefix(path, ".") {
			return nil
		}

		res = append(res, upload(b, path))

		return nil
	})

	return
}

// upload one file
func upload(b Bed, filePath string) (url string) {
	url, err := b.UploadByPath(filePath)
	if err != nil {
		log.Printf("[%s]: Upload failed, reason -> %v", filePath, err)
	} else {
		log.Printf("[%s]: Upload finished, url -> %s", filePath, url)
	}

	return
}
